import './App.css';

// UserContext
import { UserProvider } from './UserContext';

// when the dev wants to render multiple components inside a reuseable function such as App, Fragment component of react is used to enclosed these components to avoid errors in our frontend application; the pair of <> and </> can also be used
import { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

// Web Components
import AppNavbar from './AppNavbar';

// Web Pages

import Home from "./pages/Home";
import Courses from "./pages/Courses";
import CourseView from './components/CourseView';
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";

/*
  jsx are similar to HTML tags, with one major difference taht is being able to apply Javascript code
*/

/*
  react-router-dom - allows us to simulate changing pages in react. by default, react is used for SPA - Single Page Application
  Router(BrowserRouter) - used to wrap components that uses react-router-dom and allows the use of routes and the routing system. not wrapping the Routes and Route compoonents will render and error since the dev tries to use them without the BrowserRouter dependency

  - Routes - holds all the Route components (it is used to wrap Route elements that are meant to hold the endpoint and the component to be rendered in the page)

  Route - assigns an endpoint and displays the appropriate page component for that endpoint
      path - assigns the endpoint throught string data type
      element attribute - assigns the page component to be rendered/displayed at that particular endpoint
*/
function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(()=>{
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/courses' element={<Courses />} />
          <Route path='/courses/:courseId' element={<CourseView />} />
          <Route path='/login' element={<Login />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/register' element={<Register />} />
          <Route path='*' element={<Error />} />
        </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>
  );
}

export default App;