import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';


/*
	in App.js, add the unsetUser to be used by other components in the web app

	import the following
	useContext, useEffect and UserContext

	create a unsetUser variable that has the value coming from the UserContext

	use the useEffect and setUser to set the user value into null (this should be in a form of object)

	delete the localStorage.clear(); method

	send the screenshot of both logged in and not logged in user in the batch google chat

	15mins: 6:57 pm
*/

export default function Logout() {
	// consumes the UserContext object and destructure it to access the unsetUser and setUser states
	const { unsetUser, setUser } = useContext(UserContext);

	// clear the localStorage
	unsetUser();

	/*
		placing the "setUser" setter function inside of a useEffect is necessary because of the updates in React JS that a state of another component cannot be updated while trying to render a different component
		by adding useEffect, this will render Logout page first before triggering the useEffect which changes the state of the user
	*/
	useEffect(()=>{
		// Set the user state back to it's original state
		setUser({id: null});
	})

	return(
		<Navigate to='/login' />
	)
}