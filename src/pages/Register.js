import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

	const { user } = useContext(UserContext);

		/*
			Miniactivity 
				1 create email, password1 and password2 variables using useState and set their inital value to empty string ""
				2 create isActive variable using useState and set the initial value to false
				3 refactor the button and add a ternary operator stating that if the isActive is true, the button will be clickable, otherwise, disabled

				6 minutes: 6:30 pm
		*/
		// state hooks to store the values of the input fields
		const [firstName, setFirstName] = useState('');
		const [lastName, setLastName] = useState('');
		const [email, setEmail] = useState('');
		const [mobileNo, setMobileNo] = useState('');
		const [password1, setPassword1] = useState('');
		const [password2, setPassword2] = useState('');
		// state to determine whether the submit button is enabled or not
		const [isActive, setIsActive] = useState(false);

		// to check if we have successfully implemented the 2-way binding
		/*
			whenever a state of a component changes, the component renders the whole component executing the code found in the component itself.
			e.target.value allows us to gain access the input field's current value to be used when submitting form data
		*/
		console.log(firstName);
		console.log(lastName);
		console.log(email);
		console.log(mobileNo);
		console.log(password1);
		console.log(password2);
		/*
			Miniactivity 10 minutes
				using useEffect, create a function that will trigger the submit button to be enabled once the conditions have met.
					if:
					- the input fields have to be filled with information from the user
					- password1 and password2 should match
						change the value of setIsActive into true
					-otherwise:
						setIsActive value into false

				the function should listen to email, password1 and password2 variables
				Send the screenshot in the google chat
		*/
	useEffect(()=>{
		if( ( firstName !== '' && lastName !=='' && email !== '' && mobileNo !=='' && password1 !== '' && password2 !== '' ) && ( password1 === password2 ) && (mobileNo.length > 10)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [ firstName, lastName, email, mobileNo, password1, password2 ]);

	function registerUser(e){
		

		fetch('http://localhost:4000/users/checkEmail',{
			method: 'POST',
			headers:{
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		// good practice to output the response in the console to check if the codes are working
		.then(data => {
			console.log(data);

			if (data === false ) {


				Swal.fire({
					title:"Registration Successful!",
					icon: "sucess",
					text: "Welcome to Zuitt!"
				}); 


			} 	else if (data === false ) {
				<Navigate to='/login' />

			}
			
			
			else{
				Swal.fire({
					title: "Registration Failed.",
					icon: "error",
					text: "This user already exits."
				})
			}
		});

		// alert('Thank you for registering!');


		e.preventDefault();

		// clear input fields
		/*
			setter functions can also bring back the original states of the getter variables if the dev wants to
		*/
		setFirstName('');
		setLastName('');
		setEmail('');
		setMobileNo('');
		setPassword1('');
		setPassword2('');
	}



	return (
		(user.email === null) ?
		<Navigate to='/courses' />
		:
		
	  <Form onSubmit={(e) => registerUser(e)}>

	  	<Form.Group controlId="userFirstName" className='m-2'>
	      <Form.Label>First Name</Form.Label>
	      <Form.Control 
	      				type="text" 
	      				placeholder="First Name" 
	      				value={firstName}
	      				onChange={e => setFirstName(e.target.value)}
	      				required 
			/>
	    </Form.Group>
	  	<Form.Group controlId="userLastName" className='m-2'>
	      <Form.Label>Last Name</Form.Label>
	      <Form.Control 
	      				type="text" 
	      				placeholder="Last Name" 
	      				value={lastName}
	      				onChange={e => setLastName(e.target.value)}
	      				required 
			/>
	    </Form.Group>
	    <Form.Group controlId="userEmail" className='m-2'>
	      <Form.Label>Email address</Form.Label>
	      <Form.Control 
	      				type="email" 
	      				placeholder="Enter email" 
	      				value={email}
	      				onChange={e => setEmail(e.target.value)}
	      				required 
			/>
	      <Form.Text className="text-muted">
	        We'll never share your email with anyone else.
	      </Form.Text>
	    </Form.Group>

	  	<Form.Group controlId="userMobileNo" className='m-2'>
	      <Form.Label>Mobile Number</Form.Label>
	      <Form.Control 
	      				type="number" 
	      				placeholder="09123456789" 
	      				value={mobileNo}
	      				onChange={e => setMobileNo(e.target.value)}
	      				required 
			/>
	    </Form.Group>
	    <Form.Group controlId="password1" className='m-2'>
	      <Form.Label>Password</Form.Label>
	      <Form.Control
	      				type="password" 
	      				placeholder="Password" 
	      				value={password1}
	      				onChange={e => setPassword1(e.target.value)}
	      				required
			/>
	    </Form.Group>

	    <Form.Group controlId="password2" className='m-2'>
	      <Form.Label>Verify Password</Form.Label>
	      <Form.Control 
	      				type="password" 
	      				placeholder="Password" 
	      				value={password2}
	      				onChange={e => setPassword2(e.target.value)}
	      				required
			/>
	    </Form.Group>
	    {isActive ?
	    <Button variant="primary" type="submit" id="submitBtn" className='m-2'>Submit</Button>
	    :
	    <Button variant="primary" type="submit" id="submitBtn" className='m-2' disabled>Submit</Button>
		}
	  </Form>
	);
}